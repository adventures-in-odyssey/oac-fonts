# OAC-FONTS

Custom fonts for Odyssey Adventure Club

## Description
* This repo will compile a new font set based on SVGs in [src](src) folder
* Font preview can be found [here](https://adventures-in-odyssey.gitlab.io/oac-fonts/)

## Requirements
1. You need [FontCustom](https://github.com/FontCustom/fontcustom) to be able to compile the font file

## Usage
* Just go into ${repolocation} and run compile

```bash
cd ~/${repolocation}
fontcustom compile
```
